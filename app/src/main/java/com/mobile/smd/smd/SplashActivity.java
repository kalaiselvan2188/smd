package com.mobile.smd.smd;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.smd.smd.utils.SpHelper;


public class SplashActivity extends AppCompatActivity {
    private static final int SPLASH_LOAD_TIME = 3500;
    private boolean isUserLoggedIn = false;
    private boolean isFirstTime = true;
    private SpHelper spHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        TextView versionText = (TextView) findViewById(R.id.txt_version);
        versionText.setText("V " + BuildConfig.VERSION_NAME);
        spHelper = new SpHelper(getApplicationContext());
        isFirstTime = spHelper.isFirstTime();
    }
    @Override
    protected void onStart() {
        super.onStart();
        checkForLoginStatus();
    }

    private void checkForLoginStatus() {
        if (isFirstTime) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showLoginForm();
                }
            }, SPLASH_LOAD_TIME);
        } else {
            if (isUserLoggedIn) {
                startActivity(new Intent(this, HomeActivity.class));
                finish();
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showLoginForm();
                    }
                }, SPLASH_LOAD_TIME);
            }
        }
    }

    private void showLoginForm() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }
}
