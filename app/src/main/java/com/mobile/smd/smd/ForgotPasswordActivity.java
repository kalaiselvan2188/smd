package com.mobile.smd.smd;

import android.content.Intent;
import android.support.transition.Visibility;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ForgotPasswordActivity extends AppCompatActivity  implements View.OnClickListener{
    Button btnSubmit;
    Button btnBack;
    Button btnresetconfirm;
    Button btnresetcancel;
    private EditText txtVerification;
    private TextView lblVerification;
    private TextView lblPassword;
    private TextView lblConfirmPassword;
    private EditText txtPassword;
    private EditText txtRenterPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        txtVerification = (EditText) findViewById(R.id.txtVerification);
        lblVerification = (TextView) findViewById(R.id.lblVerification);
        lblPassword = (TextView) findViewById(R.id.lblPassword);
        lblConfirmPassword = (TextView) findViewById(R.id.lblConfirmPassword);
        txtRenterPassword = (EditText) findViewById(R.id.txtRenterPassword);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnSubmit = (Button) findViewById(R.id.btn_submit);
        btnBack = (Button) findViewById(R.id.btn_back);
        btnSubmit.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        btnresetconfirm = (Button) findViewById(R.id.btn_reset_confirm);
        btnresetcancel = (Button) findViewById(R.id.btn_reset_cancel);
        btnresetconfirm.setOnClickListener(this);
        btnresetcancel.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btn_submit:
                if (txtVerification.getText().length()> 0) {
                    txtVerification.setVisibility(View.GONE);
                    btnSubmit.setVisibility(View.GONE);
                    btnBack.setVisibility(View.GONE);
                    lblVerification.setVisibility(View.GONE);
                    btnresetconfirm.setVisibility(View.VISIBLE);
                    btnresetcancel.setVisibility(View.VISIBLE);
                    txtRenterPassword.setVisibility(View.VISIBLE);
                    txtPassword.setVisibility(View.VISIBLE);
                    lblPassword.setVisibility(View.VISIBLE);
                    lblConfirmPassword.setVisibility(View.VISIBLE);
                }
                else
                    txtVerification.setError("Please enter verification code");
                break;
            case R.id.btn_back:
                intent =new Intent(ForgotPasswordActivity.this,LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_reset_confirm:
                if(txtPassword.getText().toString().trim().length() > 0 && txtRenterPassword.getText().toString().trim().length() > 0 )
                {
                    if (txtPassword.getText().toString().equals(txtRenterPassword.getText().toString()))
                    {
                        intent =new Intent(ForgotPasswordActivity.this,HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        txtRenterPassword.setError("Please enter password same as above");
                    }
                }
                else {
                    if (txtPassword.getText().length() == 0)
                        txtPassword.setError("Please enter password");

                    if (txtRenterPassword.getText().length() == 0)
                        txtRenterPassword.setError("Please re-enter password");
                }
                break;
            case R.id.btn_reset_cancel:
                intent =new Intent(ForgotPasswordActivity.this,LoginActivity.class);
                startActivity(intent);
                break;
        }
    }
}
