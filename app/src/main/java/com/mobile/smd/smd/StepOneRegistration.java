package com.mobile.smd.smd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class StepOneRegistration extends AppCompatActivity  {
    private EditText phoneNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_one_registration);
        Button btnLogin = (Button) findViewById(R.id.btn_generateOTP);
        phoneNumber = (EditText) findViewById(R.id.txtPhoneNumber);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(phoneNumber.getText().toString().trim().length() > 0)
                {
                    if(android.util.Patterns.PHONE.matcher("+91"+phoneNumber.getText().toString()).matches() && phoneNumber.getText().toString().trim().length()==10) {
                        Intent intent = new Intent(StepOneRegistration.this, StepTwoRegistration.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                        phoneNumber.setError("Please enter valid phone number");
                }
                else
                {
                    if (phoneNumber.getText().length() == 0)
                        phoneNumber.setError("Please enter phone number");

                }
            }
        });
    }
}
