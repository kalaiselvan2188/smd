package com.mobile.smd.smd;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class PostRegistrationForm extends AppCompatActivity {

    private ImageButton icon_field, icon_degree,icon_specialty,icon_year,icon_sem,icon_topicgood,icon_topichelp;
    private Button btn_apply;
    private TextView txt_field, txt_degree,txt_specialty,txt_year,txt_sem,txt_topicgood,txt_topichelp;
    private final int field_request_Code=1988;
    private final int degree_request_Code=1989;
    private final int specialty_request_Code=1990;
    private final int year_request_Code=1991;
    private final int sem_request_Code=1992;
    private final int good_request_Code=1993;
    private final int help_request_Code=1994;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_registration_form);

        icon_field = (ImageButton) findViewById(R.id.icon_field);
        icon_degree = (ImageButton) findViewById(R.id.icon_degree);
        icon_specialty = (ImageButton) findViewById(R.id.icon_specialty);
        icon_year = (ImageButton) findViewById(R.id.icon_year);
        icon_sem = (ImageButton) findViewById(R.id.icon_sem);
        icon_topicgood = (ImageButton) findViewById(R.id.icon_topicgood);
        icon_topichelp = (ImageButton) findViewById(R.id.icon_topichelp);

        btn_apply = (Button) findViewById(R.id.btn_apply);

        txt_field = (TextView) findViewById(R.id.txt_field);
        txt_degree = (TextView) findViewById(R.id.txt_degree);
        txt_specialty = (TextView) findViewById(R.id.txt_specialty);
        txt_year = (TextView) findViewById(R.id.txt_year);
        txt_sem = (TextView) findViewById(R.id.txt_sem);
        txt_topicgood = (TextView) findViewById(R.id.txt_topicgood);
        txt_topichelp = (TextView) findViewById(R.id.txt_topichelp);

        icon_field.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFieldPopup(field_request_Code,"Field");
            }
        });
        icon_degree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFieldPopup(degree_request_Code,"Degree");
            }
        });
        icon_specialty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFieldPopup(specialty_request_Code,"Specialty");
            }
        });
        icon_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFieldPopup(year_request_Code,"Year");
            }
        });
        icon_sem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showFieldPopup(sem_request_Code,"Semester");
            }
        });
        icon_topicgood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFieldPopup(good_request_Code,"Topicgoodat");
            }
        });
        icon_topichelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFieldPopup(help_request_Code,"Topichelpat");
            }
        });
        btn_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txt_sem.getText().toString().trim().length() > 0 &&txt_field.getText().toString().trim().length() > 0 && txt_degree.getText().toString().trim().length() > 0 && txt_specialty.getText().toString().trim().length() > 0 && txt_year.getText().toString().trim().length() > 0&& txt_sem.getText().toString().trim().length() > 0)
                {
                    Intent intent =new Intent(PostRegistrationForm.this,HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    if (txt_field.getText().length() == 0)
                        txt_field.setError("Please select field");

                    if (txt_degree.getText().length() == 0)
                        txt_degree.setError("Please select degree");

                    if (txt_specialty.getText().length() == 0)
                        txt_specialty.setError("Please select specialty");

                    if (txt_year.getText().length() == 0)
                        txt_year.setError("Please select year");
                    if (txt_sem.getText().length() == 0)
                        txt_sem.setError("Please select semester");

                }

            }
        });

    }

    private void showFieldPopup(int field_request_Code, String loadData) {
        Intent intent = new Intent(this, SelectFieldActivity.class);
        intent.putExtra("Load Field",loadData);
        startActivityForResult(intent, field_request_Code);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            String[]  returnedResult;
            switch (requestCode) {
                case field_request_Code:
                    returnedResult  = data.getStringArrayExtra("Selected Field");
                    txt_field.setText(returnedResult[0]);
                    break;
                case degree_request_Code:
                      returnedResult = data.getStringArrayExtra("Selected Field");
                    txt_degree.setText(returnedResult[0]);
                    break;
                case specialty_request_Code:
                      returnedResult = data.getStringArrayExtra("Selected Field");
                    txt_specialty.setText(returnedResult[0]);
                    break;
                case year_request_Code:
                      returnedResult = data.getStringArrayExtra("Selected Field");
                    txt_year.setText(returnedResult[0]);
                    break;
                case sem_request_Code:
                   returnedResult = data.getStringArrayExtra("Selected Field");
                    txt_sem.setText(returnedResult[0]);
                    break;
                case good_request_Code:
                     returnedResult = data.getStringArrayExtra("Selected Field");
                    if(returnedResult.length>1)
                    txt_topicgood.setText(returnedResult[0]+"...");
                    else
                        txt_topicgood.setText(returnedResult[0]);
                    break;
                case help_request_Code:
                     returnedResult = data.getStringArrayExtra("Selected Field");
                    if(returnedResult.length>1)
                    txt_topichelp.setText(returnedResult[0]+"...");
                    else
                        txt_topichelp.setText(returnedResult[0]);
                    break;
            }


            }
        }
    }


