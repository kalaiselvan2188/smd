package com.mobile.smd.smd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.soundcloud.android.crop.Crop;

public class AskQuestionActivity extends AppCompatActivity  implements View.OnClickListener{
    Button btnSubmit;
    private EditText txttag;
    private EditText txttopic;
    private EditText txtdescription;
    private EditText txtquestion;
    private ImageView imgView;
    private CardView cardView;
    private boolean hasImageChanged = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_question);
        txttag = (EditText) findViewById(R.id.txt_tag);
        txttopic = (EditText) findViewById(R.id.txt_topic);
        txtdescription = (EditText) findViewById(R.id.txt_description);
        txtquestion = (EditText) findViewById(R.id.txt_question);
        cardView = (CardView) findViewById(R.id.cardView);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
        findViewById(R.id.imageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Crop.pickImage(AskQuestionActivity.this);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                if(txtquestion.getText().toString().trim().length() > 0 && txtdescription.getText().toString().trim().length() > 0 && txttopic.getText().toString().trim().length() > 0)
                {
                    Intent intent =new Intent(AskQuestionActivity.this,HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    if (txtquestion.getText().length() == 0)
                        txtquestion.setError("Please enter question");

                    if (txtdescription.getText().length() == 0)
                        txtdescription.setError("Please enter description");

                    if (txttopic.getText().length() == 0)
                        txttopic.setError("Please enter topic");
                }
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result) {
       if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, result);
        }
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            cardView.setVisibility(View.VISIBLE);
            imgView.setImageURI(Crop.getOutput(result));
            hasImageChanged = true;
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
