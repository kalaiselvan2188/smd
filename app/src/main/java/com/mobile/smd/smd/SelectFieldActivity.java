package com.mobile.smd.smd;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class SelectFieldActivity extends AppCompatActivity  {
    ListView listView;
    Button btnApply;
    ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = this.getIntent();
        setContentView(R.layout.activity_select_field);
        listView = (ListView) findViewById(R.id.list_view);
        btnApply = (Button) findViewById(R.id.btn_apply);
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SparseBooleanArray checked = listView.getCheckedItemPositions();
                ArrayList<String> selectedItems = new ArrayList<String>();
                for (int i = 0; i < checked.size(); i++) {
                    // Item position in adapter
                    int position = checked.keyAt(i);
                    // Add sport if it is checked i.e.) == TRUE!
                    if (checked.valueAt(i))
                        selectedItems.add(adapter.getItem(position));
                }

                String[] outputStrArr = new String[selectedItems.size()];

                for (int i = 0; i < selectedItems.size(); i++) {
                    outputStrArr[i] = selectedItems.get(i);
                }

                Intent intent = new Intent();
                //---set the data to pass back---
                intent.putExtra("Selected Field",outputStrArr);
                setResult(RESULT_OK, intent);
                //---close the activity---
                finish();
            }
        });
        String[] field;

if(intent.getExtras().getString("Load Field").equals("Field"))
{
        field = getResources().getStringArray(R.array.Field_array);
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_single_choice, field);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

} else if (intent.getExtras().getString("Load Field").equals("Degree"))
{
    field = getResources().getStringArray(R.array.Degree_array);
    adapter = new ArrayAdapter<String>(this,
            android.R.layout.simple_list_item_single_choice, field);
    listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
} else if (intent.getExtras().getString("Load Field").equals("Specialty"))
{
    field = getResources().getStringArray(R.array.Specialty_array);
    adapter = new ArrayAdapter<String>(this,
            android.R.layout.simple_list_item_single_choice, field);
    listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
} else if (intent.getExtras().getString("Load Field").equals("Year"))
{
    field = getResources().getStringArray(R.array.Year_array);
    adapter = new ArrayAdapter<String>(this,
            android.R.layout.simple_list_item_single_choice, field);
    listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
} else if (intent.getExtras().getString("Load Field").equals("Semester"))
{
    field = getResources().getStringArray(R.array.Sem_array);
    adapter = new ArrayAdapter<String>(this,
            android.R.layout.simple_list_item_single_choice, field);
    listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
} else if (intent.getExtras().getString("Load Field").equals("Topichelpat") || intent.getExtras().getString("Load Field").equals("Topicgoodat"))
{
    field = getResources().getStringArray(R.array.Topics_array);
    adapter = new ArrayAdapter<String>(this,
            android.R.layout.simple_list_item_multiple_choice, field);
    listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
}
        listView.setAdapter(adapter);

    }

}
