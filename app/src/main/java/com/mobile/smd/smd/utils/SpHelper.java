package com.mobile.smd.smd.utils;

import android.arch.lifecycle.BuildConfig;
import android.content.Context;
import android.content.SharedPreferences;
import com.github.gfx.util.encrypt.EncryptedSharedPreferences;
import com.github.gfx.util.encrypt.Encryption;
/**
 * Created by kalaiselvanv on 11/12/2017.
 */

public class SpHelper {
    private static final String KEY_FIRST_TIME = "is_first_time";
    private static final String KEY_FIRST_LOGIN = "is_first_login";
    private SharedPreferences prefs;
    public Context context;
    public SpHelper(Context context) {
        this.context = context.getApplicationContext();
        prefs = new EncryptedSharedPreferences(Encryption.getDefaultCipher(), context);
    }

    public boolean isFirstTime() {
        int versionCode = BuildConfig.VERSION_CODE;
        int alreadySeenThisVersion = prefs.getInt(KEY_FIRST_TIME, 0);
        return versionCode != alreadySeenThisVersion;
    }

    public void setIsFirstTime() {
        int versionCode = BuildConfig.VERSION_CODE;
        prefs.edit().putInt(KEY_FIRST_TIME, versionCode).apply();

    }
}
