package com.mobile.smd.smd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class StepTwoRegistration extends AppCompatActivity  implements View.OnClickListener {
    Button btnSubmit;
    Button btnBack;
    private EditText txtVerification;
    private EditText txtName;
    private EditText txtEmail;
    private EditText txtPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_two_registration);
        txtVerification = (EditText) findViewById(R.id.txtVerification);
        txtName = (EditText) findViewById(R.id.txtName);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnSubmit = (Button) findViewById(R.id.btn_submit);
        btnBack = (Button) findViewById(R.id.btn_back);
        btnSubmit.setOnClickListener(this);
        btnBack.setOnClickListener(this);
    }
    private void Validate()
    {

        if(txtVerification.getText().toString().trim().length() > 0 && txtName.getText().toString().trim().length() > 0 && txtEmail.getText().toString().trim().length() > 0 && txtPassword.getText().toString().trim().length() > 0)
        {
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if(txtEmail.getText().toString().matches(emailPattern)) {
                Intent intent = new Intent(StepTwoRegistration.this, PostRegistrationForm.class);
                startActivity(intent);
                finish();
            }
            else
                txtEmail.setError("Please enter valid email");
        }
        else
        {
            if (txtVerification.getText().length() == 0)
                txtVerification.setError("Please enter verification code");

            if (txtName.getText().length() == 0)
                txtName.setError("Please enter name");

            if (txtEmail.getText().length() == 0)
                txtEmail.setError("Please enter email");

            if (txtPassword.getText().length() == 0)
                txtPassword.setError("Please enter password");

        }

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                Validate();
                break;
            case R.id.btn_back:
                Intent intent =new Intent(StepTwoRegistration.this,StepOneRegistration.class);
                startActivity(intent);
                break;
        }
    }
}
