package com.mobile.smd.smd;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends Activity implements View.OnClickListener {
    private Button btnaskquestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        btnaskquestion = (Button) findViewById(R.id.btn_ask_question);
        btnaskquestion.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ask_question:
                    Intent intent =new Intent(HomeActivity.this,AskQuestionActivity.class);
                    startActivity(intent);
                    finish();
                break;
        }
        }

    }
